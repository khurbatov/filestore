package ru.lanit.filestore;

import org.junit.Test;

public class SimpleClientTest {

	@Test
	public void testClient() {
		SimpleClient client = new SimpleClient();
		client.connect("172.29.73.37");
		client.getSession();
		client.createSchema();
		for (int i = 0; i < 10000; i++) {
			client.loadData();
			System.out.println(i);
		}
		client.querySchema();
		client.closeSession();
		client.close();
	}
}