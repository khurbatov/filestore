package ru.lanit.filestore;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import ru.lanit.filestore.FilestoreService.Writer;

public class FilestoreTest {

	static FilestoreService service = new FilestoreService();
	ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors
			.newFixedThreadPool(20);

	public static void main(String[] args) throws IOException, InterruptedException {
		FilestoreTest test = new FilestoreTest();
		test.testWriteFs();
	}

	@BeforeClass
	public static void setUp() {
		service.createSchema();
	}

	@AfterClass
	public static void tearDown() {
		// SessionPool.getInstance().close();
	}

	@Test
	public void testWriteFs() throws IOException, InterruptedException {
		FileInputStream fin = null;
		long start = System.currentTimeMillis();
		List<Callable<Boolean>> futures = new ArrayList<>();
		for (int i = 0; i < 20; i++) {
			futures.add(new Callable<Boolean>() {
				@Override
				public Boolean call() {
					for (int k = 0; k < 50; k++) {
						try {
							FileInputStream fin = new FileInputStream(
									"./test_git.pdf");
							service.storeFile2(fin);
							fin.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					return true;
				}
			});
			executor.invokeAll(futures);
		}

		 /*for (int i = 0; i < 1000; i++) { 
			 fin = new FileInputStream("./test_git.pdf"); service.storeFile(fin); 
		  }*/
		 
		System.out.println("Finish write - "
				+ TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()
						- start) + " sec");

	}

	// @Test
	public void testReadFs() {
		long start = System.currentTimeMillis();

		List<UUID> allFiles = service.getAllFiles();

		allFiles.forEach(uuid -> {
			try {
				service.loadFile(uuid);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});

		System.out.println("Finish read - "
				+ TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()
						- start) + " sec");
	}
}
