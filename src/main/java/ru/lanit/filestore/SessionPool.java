package ru.lanit.filestore;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.Session;

public class SessionPool {

	private Cluster cluster;

	private Map<String, Session> inUse = new HashMap<>();
	private int sessionCount = 10;

	private BlockingQueue<Session> sessions = new ArrayBlockingQueue<Session>(
			sessionCount);

	private SessionPool() {
		connect("172.29.73.37");
	}

	private static class SessionPoolHolder {
		public static final SessionPool HOLDER_INSTANCE = new SessionPool();
	}

	public static SessionPool getInstance() {
		return SessionPoolHolder.HOLDER_INSTANCE;
	}

	private void connect(String node) {
		cluster = Cluster.builder().addContactPoint(node).build();
		Metadata metadata = cluster.getMetadata();
		System.out.println("Connected to cluster:" + metadata.getClusterName());
		for (Host host : metadata.getAllHosts()) {
			System.out.println("Datatacenter: " + host.getDatacenter()
					+ "; Host: " + host.getAddress() + "; Rack: "
					+ host.getRack());
		}
		for (int i = 0; i < sessionCount; i++) {
			sessions.add(cluster.connect());
		}
	}

	public Session getSession() {
		String threadName = Thread.currentThread().getName();
		if (inUse.get(threadName) != null) {
			return inUse.get(threadName);
		}
		Session session = null;
		try {
			session = sessions.take();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		synchronized (this) {
			inUse.put(threadName, session);
		}
		return session;
	}

	public synchronized void releaseSession() {
		Session session = inUse.remove(Thread.currentThread().getName());
		sessions.add(session);
	}

	public void close() {
		sessions.forEach(session -> session.close());
		cluster.close();
	}

}
