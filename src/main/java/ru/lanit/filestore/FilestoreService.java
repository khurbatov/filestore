package ru.lanit.filestore;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

public class FilestoreService {

	public int CHUNK_SIZE = 262144; /*2097152;*/ // 2mb
	ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors
			.newFixedThreadPool(20);

	private Cluster cluster;

	public FilestoreService() {
		cluster = Cluster.builder().addContactPoint("172.29.73.38").build();
		Metadata metadata = cluster.getMetadata();
		System.out.println("Connected to cluster:" + metadata.getClusterName());
		for (Host host : metadata.getAllHosts()) {
			System.out.println("Datatacenter: " + host.getDatacenter()
					+ "; Host: " + host.getAddress() + "; Rack: "
					+ host.getRack());
		}
	}
	
	public void storeFile2(InputStream stream) throws IOException {
		Map<Integer, UUID> chunkOrder = new HashMap<>();
		byte[] b = new byte[CHUNK_SIZE];
		int i = 0;
		int k = 0;
		while ((k = stream.read(b, 0, CHUNK_SIZE)) != -1) {
			UUID uuid = UUID.randomUUID();
			ByteBuffer buffer = ByteBuffer.wrap(b, 0, k);
			chunkOrder.put(i, uuid);
			i++;
			Session session = cluster.connect();
			session.execute(
					"INSERT INTO filestore.documents_data(chunk_id, chunk) VALUES(?,?)",
					uuid, buffer);
			session.close();
			System.out.print(uuid.toString() + " - finish write time - ");
			System.out.println(new Date());
		}

		Session session = cluster.connect();
		session.execute(
				"INSERT INTO filestore.documents(document_id, chunks) VALUES(?,?)",
				UUID.randomUUID(), chunkOrder);
		session.close();
	}

	public void storeFile(InputStream stream) throws IOException {
		List<Future<UUID>> listFuture = new ArrayList<Future<UUID>>();
		Map<Integer, UUID> chunkOrder = new HashMap<>();
		byte[] b = new byte[CHUNK_SIZE];
		int i = 0;
		int k = 0;
		while ((k = stream.read(b, 0, CHUNK_SIZE)) != -1) {
			UUID uuid = UUID.randomUUID();
			ByteBuffer buffer = ByteBuffer.wrap(b, 0, k);
			Writer2 writer = new Writer2(buffer, uuid);
			listFuture.add(executor.submit(writer));
			chunkOrder.put(i, uuid);
			i++;
		}
		/*UUID uuid = UUID.randomUUID();
		ByteBuffer buffer = ByteBuffer.wrap(uuid.toString().getBytes());
		Writer2 writer = new Writer2(buffer, uuid);
		listFuture.add(executor.submit(writer));
		chunkOrder.put(i, uuid);
		i++;*/
		for (Future<UUID> fut : listFuture) {
			try {
				System.out.print(fut.get() + " - finish write time - ");
				System.out.println(new Date());
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}

		Session session = cluster.connect();
		session.execute(
				"INSERT INTO filestore.documents(document_id, chunks) VALUES(?,?)",
				UUID.randomUUID(), chunkOrder);
		session.close();
	}

	class Writer implements Callable<UUID> {
		ByteBuffer buffer;
		UUID uuid;

		public Writer(byte[] b, UUID uuid) {
			buffer = ByteBuffer.wrap(b);
			// buffer.put(uuid.toString().getBytes());
			this.uuid = uuid;
		}

		@Override
		public UUID call() {
			Session session = SessionPool.getInstance().getSession();
			SessionPool.getInstance().getSession();
			session.execute(
					"INSERT INTO filestore.documents_data(chunk_id, chunk) VALUES(?,?)",
					uuid, buffer);
			SessionPool.getInstance().releaseSession();
			return uuid;
		}
	}

	class Writer2 implements Callable<UUID> {
		ByteBuffer buffer;
		UUID uuid;

		public Writer2(ByteBuffer buffer, UUID uuid) {
			this.buffer = buffer;
			// buffer.put(uuid.toString().getBytes());
			this.uuid = uuid;
		}

		@Override
		public UUID call() {
			Session session = cluster.connect();
			session.execute(
					"INSERT INTO filestore.documents_data(chunk_id, chunk) VALUES(?,?)",
					uuid, buffer);
			session.close();
			return uuid;
		}
	}
	
	public List<UUID> getAllFiles() {
		Session session = cluster.connect();
		List<UUID> result = new ArrayList<>();
		ResultSet rs = session
				.execute("SELECT document_id FROM filestore.documents");
		rs.forEach(row -> result.add(row.getUUID("document_id")));
		return result;
	}

	public void loadFile(UUID documentId) throws IOException, InterruptedException, ExecutionException {
		Map<Integer, Future<byte[]>> listFuture = new HashMap<Integer, Future<byte[]>>();
		Session session = cluster.connect();
		ResultSet rs = session
				.execute("SELECT chunks FROM filestore.documents WHERE document_id = " + documentId);
		Map<Integer, UUID> chunkOrder = rs.one().getMap("chunks",Integer.class, UUID.class);
		chunkOrder.forEach((k, v) -> {
			Reader reader = new Reader(v);
			Future<byte[]> future = executor.submit(reader);
			listFuture.put(k, future);
		});
		FileOutputStream stream = new FileOutputStream("D://test.pdf");
		for (int k=0; k < listFuture.size(); k++) {
			Future<byte[]> future = listFuture.get(k);
			byte[] b = future.get();
			stream.write(b);
		}
		stream.close();
		session.close();
	}

	class Reader implements Callable<byte[]> {
		ByteBuffer buffer;
		UUID uuid;

		public Reader(UUID uuid) {
			this.uuid = uuid;
		}

		@Override
		public byte[] call() {
			Session session = cluster.connect();
			ResultSet rs = session
					.execute(
							"SELECT chunk FROM filestore.documents_data WHERE chunk_id = ?",
							uuid);
			Row row = rs.one();
			ByteBuffer bbuf = row.getBytes("chunk");
			byte[] b = new byte[bbuf.remaining()];
			bbuf.get(b);
			session.close();
			return b;
		}
	}

	public void createSchema() {
		Session session = cluster.connect();
		session.execute("CREATE KEYSPACE IF NOT EXISTS filestore WITH replication "
				+ "= {'class':'SimpleStrategy', 'replication_factor':2};");
		session.execute("CREATE TABLE IF NOT EXISTS filestore.documents ("
				+ "document_id uuid, chunks map<int,uuid>, PRIMARY KEY (document_id));");
		session.execute("CREATE TABLE IF NOT EXISTS filestore.documents_data ("
				+ "chunk_id uuid," + "chunk blob," + "PRIMARY KEY(chunk_id)"
				+ ");");
		session.close();
	}

}
